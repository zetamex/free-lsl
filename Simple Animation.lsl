float anitimer = 10.0;
string aniname;
integer aniperm = FALSE;

default
{
    on_rez()
    {
        llResetScript();
    }
    
    state_entry()
    {
        aniname = llGetInventoryName(INVENTORY_ANIMATION, 0);
        llRequestPermissions(llGetOwner(), PERMISSION_TRIGGER_ANIMATION);
    }
    
    changed(integer change)
    {
        if (change & CHANGED_INVENTORY)
        {
            llResetScript();
        }
    }
    
    run_time_permissions(integer perm)
    {
        if (perm & PERMISSION_TRIGGER_ANIMATION)
        {
            llStartAnimation(aniname);
            llSetTimerEvent(anitimer);
            aniperm = TRUE;
        }
    }
    
    timer()
    {
        if (aniperm == TRUE) {
            llStartAnimation(aniname);
        }else{
            llRequestPermissions(llGetOwner(), PERMISSION_TRIGGER_ANIMATION);
        }
    }
}