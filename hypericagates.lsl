

string SimName = ""; //Replace with name of your destination
string SimAddress = ""; //Replace with hypergrid address of your destination
//Note: If your gate is on the same grid as the destination, you might need to use the region name instead of the hypergrid address 


vector LandingPoint = <128.0, 128.0, 36.0>;
vector LookAt       = <1.0,1.0,0.0>;
vector Coordinates;

list LastFewAgents;

PerformTeleport(key WhomToTeleport) {
  integer CurrentTime = llGetUnixTime();
  integer AgentIndex = llListFindList(LastFewAgents, [WhomToTeleport]);
  if (AgentIndex != -1)
  {
    integer PreviousTime = llList2Integer(LastFewAgents, AgentIndex+1);
    if (PreviousTime >= (CurrentTime - 5)) return;
    LastFewAgents = llDeleteSubList(LastFewAgents, AgentIndex, AgentIndex+1);
  }
  LastFewAgents += [WhomToTeleport, CurrentTime];
  if (SimName == "" && SimAddress == "") {
    SimName = llGetObjectName();
    SimAddress = llGetObjectDesc();
  }
  osTeleportAgent(WhomToTeleport, SimAddress, LandingPoint, LookAt);
}

default
{
    state_entry()
    {
      llSetText("Hypergate to " + SimName, <1,1,1>, 1);
      llSetTextureAnim(ANIM_ON | LOOP, 2, 4, 4, 1, 16, 6.0);
      
      // If the gate is set for Hyperica, this next section finds the closest terminal
      if (SimAddress == "hg.hyperica.com:8022:hyperica") {
        Coordinates = llGetRegionCorner();
        integer x = llRound((Coordinates.x)/256);
        integer y = llRound((Coordinates.y)/256);
        if (x>7000 && y>7000) SimAddress = "hg.hyperica.com:8022:hyperica upper";
        if (x<7000 && y<7000 && x>4000 && y>4000) SimAddress = "hg.hyperica.com:8022:hyperica central";
        if (x<4000 && y<4000) SimAddress = "hg.hyperica.com:8022:hyperica lower";
      }
    }
    touch_start(integer number) 
    {
        key toucher = llDetectedKey(0);
        PerformTeleport(toucher);
    }
    collision(integer number)
    {
        key collider = llDetectedKey(0);
        PerformTeleport(collider);
    }
}
