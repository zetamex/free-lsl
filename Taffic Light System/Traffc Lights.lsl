string apiname = "ZLAPI";
integer apichan = -9585;

integer redlightlink = 4;
integer yellowlightlink = 3;
integer greenlightlink = 2;

float intensity = 1.0;
float radius = 10.0;
float falloff = 0.7;

vector redcolour = <1.0,0.0,0.0>;
vector yellowcolour = <1.000, 0.863, 0.000>;
vector greencolour = <0.0,1.0,0.0>;

default
{
	state_entry()
	{
		llListen(apichan, "", "", "");
	}

	listen(integer channel, string name, key id, string message)
	{
	       if (channel == apichan) {
        		list apimsglist = llParseString2List(message, ["="], []);
                	string msg0 = llList2String(apimsglist, 0);
                	if (msg0 == apiname) {
                		string msg1 = llList2String(apimsglist, 1);
                		string msg2 = llList2String(apimsglist, 2);
                		if (msg1 == llGetObjectDesc()) { // for east and west put in the prim's description as EASTWEST, north/south put NORTHSOUTH
                			if (msg2 == "go") {
                				llSleep(8.0);
                				llSetLinkPrimitiveParams(redlightlink, [PRIM_FULLBRIGHT, ALL_SIDES, FALSE, PRIM_POINT_LIGHT, FALSE, <0.0,0.0,0.0>, 0.0, 0.0, 0.0]);
                				llSetLinkPrimitiveParams(yellowlightlink, [PRIM_FULLBRIGHT, ALL_SIDES, FALSE, PRIM_POINT_LIGHT, FALSE, <0.0,0.0,0.0>, 0.0, 0.0, 0.0]);
                				llSetLinkPrimitiveParams(greenlightlink, [PRIM_FULLBRIGHT, ALL_SIDES, TRUE, PRIM_POINT_LIGHT, TRUE, greencolour, intensity, radius, falloff]);
                			}
                			if (msg2 == "stop") {
                				llSetLinkPrimitiveParams(redlightlink, [PRIM_FULLBRIGHT, ALL_SIDES, FALSE, PRIM_POINT_LIGHT, FALSE, <0.0,0.0,0.0>, 0.0, 0.0, 0.0]);
                				llSetLinkPrimitiveParams(yellowlightlink, [PRIM_FULLBRIGHT, ALL_SIDES, TRUE, PRIM_POINT_LIGHT, TRUE, yellowcolour, intensity, radius, falloff]);
                				llSetLinkPrimitiveParams(greenlightlink, [PRIM_FULLBRIGHT, ALL_SIDES, FALSE, PRIM_POINT_LIGHT, FALSE, <0.0,0.0,0.0>, 0.0, 0.0, 0.0]);
                				llSleep(6.0);
                				llSetLinkPrimitiveParams(redlightlink, [PRIM_FULLBRIGHT, ALL_SIDES, TRUE, PRIM_POINT_LIGHT, TRUE, redcolour, intensity, radius, falloff]);
                				llSetLinkPrimitiveParams(yellowlightlink, [PRIM_FULLBRIGHT, ALL_SIDES, FALSE, PRIM_POINT_LIGHT, FALSE, <0.0,0.0,0.0>, 0.0, 0.0, 0.0]);
                				llSetLinkPrimitiveParams(greenlightlink, [PRIM_FULLBRIGHT, ALL_SIDES, FALSE, PRIM_POINT_LIGHT, FALSE, <0.0,0.0,0.0>, 0.0, 0.0, 0.0]);
                			}
                		}
                	}
	       }
	}
}
