string apiname = "ZLAPI";
integer apichan = -9585;

float flashtimer = 5.0; // timer in seconds till the hand starts to flash

integer stoplink = 18;
integer golink = 17;

vector stopcolour = <1.0,0.0,0.0>;
vector gocolour = <0.0,1.0,0.0>;

float intensity = 1.0;
float radius = 10.0;
float falloff = 0.7;

integer flashred = FALSE;

default
{
    state_entry()
    {
        llListen(apichan, "", "", "");
    }

    listen(integer channel, string name, key id, string message)
    {
        if (channel == apichan) {
            list apimsglist = llParseString2List(message, ["="], []);
            string msg0 = llList2String(apimsglist, 0);
            if (msg0 == apiname) {
                string msg1 = llList2String(apimsglist, 1);
                string msg2 = llList2String(apimsglist, 2);
                if (msg1 == llGetObjectDesc()) {
	                if (msg2 == "go") {
	                    llSleep(8.0);
	                    llSetLinkPrimitiveParams(stoplink, [PRIM_GLOW, ALL_SIDES, 0.0, PRIM_FULLBRIGHT, ALL_SIDES, FALSE, PRIM_POINT_LIGHT, FALSE, <0.0,0.0,0.0>, 0.0, 0.0, 0.0]);
	                    llSetLinkPrimitiveParams(golink, [PRIM_GLOW, ALL_SIDES, intensity, PRIM_FULLBRIGHT, ALL_SIDES, TRUE, PRIM_POINT_LIGHT, TRUE, gocolour, intensity, radius, falloff]);
	                    llSetTimerEvent(flashtimer);
	                }
	                if (msg2 == "stop") {
	                    llSetTimerEvent(0.0);
	                    llSetLinkPrimitiveParams(stoplink, [PRIM_GLOW, ALL_SIDES, intensity, PRIM_FULLBRIGHT, ALL_SIDES, TRUE, PRIM_POINT_LIGHT, TRUE, stopcolour, intensity, radius, falloff]);
	                    llSetLinkPrimitiveParams(golink, [PRIM_GLOW, ALL_SIDES, 0.0, PRIM_FULLBRIGHT, ALL_SIDES, FALSE, PRIM_POINT_LIGHT, FALSE, <0.0,0.0,0.0>, 0.0, 0.0, 0.0]);
	                    flashred = TRUE;
	                }
	            }
            }
        }
    }

    timer()
    {
        llSetTimerEvent(1.0);
        if (flashred) {
            flashred = FALSE;
            llSetLinkPrimitiveParams(stoplink, [PRIM_GLOW, ALL_SIDES, intensity, PRIM_FULLBRIGHT, ALL_SIDES, TRUE, PRIM_POINT_LIGHT, TRUE, stopcolour, intensity, radius, falloff]);
            llSetLinkPrimitiveParams(golink, [PRIM_GLOW, ALL_SIDES, 0.0, PRIM_FULLBRIGHT, ALL_SIDES, FALSE, PRIM_POINT_LIGHT, FALSE, <0.0,0.0,0.0>, 0.0, 0.0, 0.0]);
        }else{
            flashred = TRUE;
            llSetLinkPrimitiveParams(stoplink, [PRIM_GLOW, ALL_SIDES, 0.0, PRIM_FULLBRIGHT, ALL_SIDES, FALSE, PRIM_POINT_LIGHT, FALSE, <0.0,0.0,0.0>, 0.0, 0.0, 0.0]);
            llSetLinkPrimitiveParams(golink, [PRIM_GLOW, ALL_SIDES, 0.0, PRIM_FULLBRIGHT, ALL_SIDES, FALSE, PRIM_POINT_LIGHT, FALSE, <0.0,0.0,0.0>, 0.0, 0.0, 0.0]);
        }
    }
}