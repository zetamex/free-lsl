string apiname = "ZLAPI";
string directionstatus;
integer apichan = -9585;
integer isNorthSouthRed = TRUE; // If north and south lights are red then send the message to turn green
float lightchangetimer = 30;
vector directioncolour;

key owner;
key primkey;
integer localchan;
integer glisten;

changetrafficlights()
{
	string sendmsg1;
	string sendmsg2;
	if (isNorthSouthRed) {
		sendmsg1 = apiname+"=NORTHSOUTH=go";
		sendmsg2 = apiname+"=EASTWEST=stop";
		isNorthSouthRed = FALSE;
		directioncolour = <0.0,1.0,0.0>;
		directionstatus = "North and South";
	}else{
		sendmsg1 = apiname+"=NORTHSOUTH=stop";
		sendmsg2 = apiname+"=EASTWEST=go";
		isNorthSouthRed = TRUE;
		directioncolour = <1.0,0.0,0.0>;
		directionstatus = "East and West";
	}
	llRegionSay(apichan, sendmsg1);
	llRegionSay(apichan, sendmsg2);

	llSetText("Traffic is currently going " + directionstatus, directioncolour, 1.0);
}

default
{
	state_entry()
	{
		owner = llGetOwner();
		primkey = llGetKey();
		localchan = (integer)("0x80000000"+llGetSubString((string)primkey,-8,-1));
		llListen(apichan, "", "", "");
		llSetTimerEvent(lightchangetimer);
	}

	touch_end(integer num_detected)
	{
		key toucher = llDetectedKey(0);
		if (toucher == owner) {
			glisten = llListen(localchan, "", owner, "");
			llDialog(owner, "Traffic Control Box.", ["ALL STOP", "GO", "RESET"], localchan);
		}
	}

	listen(integer channel, string name, key id, string msg)
	{
		if (channel == localchan && id == owner) {
			if (msg == "ALL STOP") {
				string sendmsg1 = apiname+"=NORTHSOUTH=stop";
				string sendmsg2 = apiname+"=EASTWEST=stop";
				isNorthSouthRed = TRUE;
				directioncolour = <1.0,0.0,0.0>;
				llRegionSay(apichan, sendmsg1);
				llRegionSay(apichan, sendmsg2);
				llSetText("Traffic is currently stopped in all directions.", directioncolour, 1.0);
				llSetTimerEvent(0.0);
			}
			if (msg == "GO") {
				llSetTimerEvent(lightchangetimer);
			}
			if (msg == "RESET") {
				llResetScript();
			}
		}
	}

	timer()
	{
		changetrafficlights();
	}
}
