float heightpos = 2012.0; // Height to teleport the toucher to. Works kinda like doing gth and the height in Firestorm.
// Example: gth 2012 << the 2012 followed by a .0 is what you put as heightpos.

default
{
    touch_end(integer n)
    {
        key av = llDetectedKey(0);
        if (av) { // evaluated as true if key is valid and not NULL_KEY
            vector pos = llDetectedPos(0);
            vector newpos = <pos.x, pos.y, heightpos>;
            llTeleportAgent(av, "", newpos, <0,0,0>);
        }
    }
}