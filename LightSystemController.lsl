float intensity = 1.0;
float radius = 10.0;
float sunchecktimer = 30.0;

key owner;
key primkey;

integer autoon = TRUE;
integer apichan = -9548;
integer localchan;
integer gListener;
integer face = 1;

string lightingstate = "off";
string apiname = "ZLAPI";

lightchanger(string ls)
{
    if (ls == "on")
    {
        lightingstate = "on";
        llRegionSay(apichan, apiname+"=LIGHTS=on");
    }
    else if (ls == "off")
    {
        lightingstate = "off";
        llRegionSay(apichan, apiname+"=LIGHTS=off");
    }
    llMessageLinked(LINK_SET, 0, ls, "");
}

default
{
    on_rez(integer start_param)
    {
        llResetScript();
    }
    
    state_entry()
    {
        owner = llGetOwner();
        primkey = llGetKey();
        llListen(apichan, "", "", "");
        localchan = (integer)("0x80000000"+llGetSubString((string)primkey,-8,-1));
        if (autoon) {
            llSetTimerEvent(sunchecktimer);
        }else{
            llSetTimerEvent(0.0);
        }
    }
    
    touch_end(integer n)
    {
        key toucher = llDetectedKey(0);
        if (toucher == owner)
        {
            gListener = llListen(localchan, "", owner, "");
            string autostate;
            string intensitystate;
            if (autoon) {
                autostate = "On";
            }else{
                autostate = "Off";
            }
            
            if (intensity == 1.0) {
                intensitystate = "Bright";
            }else if (intensity == 0.5) {
                intensitystate = "Med";
            }else if (intensity == 0.1) {
                intensitystate = "Low";
            }else if (intensitystate == 0.0) {
                intensitystate = "Off";
            }
            llDialog(owner, "Please choose a option\nAuto on at dust is set to " + autostate + "\n Intensity set to " + intensitystate, ["Bright", "Med", "Low", "On", "Off", "Auto", "RESET", "EXIT"], localchan);
        }
    }
    
    listen(integer c, string n, key id, string msg)
    {
        if (c == localchan && id == owner) {
            if (msg == "Bright") {
                intensity = 1.0;
                lightchanger(lightingstate);
                llRegionSay(apichan, apiname+"=LIGHTS=bright");
            }
            if (msg == "Med") {
                intensity = 0.5;
                lightchanger(lightingstate);
                llRegionSay(apichan, apiname+"=LIGHTS=med");
            }
            if (msg == "Low") {
                intensity = 0.1;
                lightchanger(lightingstate);
                llRegionSay(apichan, apiname+"=LIGHTS=low");
            }
            if (msg == "On") {
                lightchanger("on");
                autoon = FALSE;
                llSetTimerEvent(0.0);
                llRegionSay(apichan, apiname+"=LIGHTS=on");
            }
            if (msg == "Off") {
                lightchanger("off");
                autoon = FALSE;
                llSetTimerEvent(0.0);
                llRegionSay(apichan, apiname+"=LIGHTS=off");
            }
            if (msg == "Auto") {
                llRegionSay(apichan, apiname+"=LIGHTS=auto");
                if (autoon) {
                    autoon = FALSE;
                    llSetTimerEvent(0.0);
                }else{
                    autoon = TRUE;
                    llSetTimerEvent(sunchecktimer);
                }
            }
            if (msg == "EXIT") {
                // do nothing
            }
            if (msg == "RESET") {
                llRegionSay(apichan, apiname+"=LIGHTS=RESET");
                llResetScript();
            }
            llMessageLinked(LINK_SET, 0, msg, "");
            llListenRemove(gListener);
        }
    }
    
    timer()
    {
        vector sun = llGetSunDirection();
        if(sun.z > 0.0) {
            lightchanger("off");
        }else{
            lightchanger("on");
        }
    }
}