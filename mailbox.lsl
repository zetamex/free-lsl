integer nccount;
key owner;
default
{
    state_entry()
    {
        owner = llGetOwner();
        llAllowInventoryDrop(TRUE);
        nccount = llGetInventoryNumber(INVENTORY_NOTECARD);
        llSetText("Drop a notecard into this prim to leave a message.\n" + (string)nccount + " messages waiting.", <1.0,1.0,1.0>, 1.0);
    }
    
    changed(integer c)
    {
        if (c & CHANGED_INVENTORY)
        {
            string invname = llGetInventoryName(INVENTORY_ALL, 0);
            integer invtype = llGetInventoryType(invname);
            if (invtype != INVENTORY_NOTECARD) {
                if (invname == llGetScriptName()) {
                    // for sure dont want to delete this script.
                }else{
                    llRemoveInventory(invname);
                    llSay(0, "Sorry that is NOT a notecard. This mailbox is for notecard messages ONLY!");
                }
            }else if (invtype == INVENTORY_NOTECARD) {
                llResetScript();
            }
        }
    }
    
    touch_end(integer n)
    {
        integer ier;
        for (ier = 0; ier < n; ier++)
        {
            key toucher = llDetectedKey(0);
            if (toucher == owner)
            {
                if (nccount == 0) {
                    llSay(0, "No messages waiting for you yet.");
                }else{
                    list inventoryItems;
                    integer i;
                    for(i; i < nccount; i++)
                    {
                        string itemName = llGetInventoryName(INVENTORY_NOTECARD, i);
                        inventoryItems += itemName;
                    }
                    llGiveInventoryList(owner, "Messages", inventoryItems);
                }
            }
        }
    }
}