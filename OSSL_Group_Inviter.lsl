// OSSL Group Invite Script
// Writen by Christopher
// For the Free LSL Scripts
// See More At: http://git.zetamexlabs.com/opensource-projects/free-lsl
// Part of Zetamex http://zetamex.com/

default
{
    touch_end(integer n)
    {
        key toucher = llDetectedKey(0);
        integer samegroup = llSameGroup(toucher);
        list details = llGetObjectDetails(llGetKey(), ([OBJECT_GROUP]));;
        key group_key = llList2String(details, 0);
        if (samegroup) {
            llSay(0, "You are already in secondlife:///app/group/" + (string)group_key + "/about");
        }else{
            osInviteToGroup(toucher);
            llSay(0, "Join secondlife:///app/group/" + (string)group_key + "/about");
        }
    }
}