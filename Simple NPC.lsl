// For more info please visit http://opensimulator.org/wiki/OSSL_Implemented
// ONLY to be used on Opensim grids with enablenpc set to true.
// this will give off tons of errors if used in second life and grids that has npc's turned off.
// Please place a appearance notecard into the prim before this script.
// The notecard can be empty.
string npcfirstname = "Mark";
string npclastname = "Nut";

// these two are automaticly detected so no need to input their names here
string appearancenotecard; // the included xml notecard for what the npc will wear.
string animation2play; // the included animation the npc will play

float range = 10.0; // range to scan for avatars in meters.
key npc; // key of the rezzed npc so we can derez the npc later.
integer avidetected = FALSE; // if avatars have already been detected dont want to rez a npc again.
integer move2avatar = FALSE; // set this to true if you want the npc to walk towards the detected avi
key detectedavi;
vector avipos;

default
{
	state_entry()
	{
		string desc = llGetObjectDesc();
		if (desc != "") {
			npc = (key)desc;
			avidetected = FALSE;
			osNpcStopAnimation(npc, animation2play);
			osNpcRemove(npc);
			npc = "";
			detectedavi = "";
			llSetObjectDesc("");
		}
		appearancenotecard = llGetInventoryName(INVENTORY_NOTECARD, 0);
		animation2play = llGetInventoryName(INVENTORY_ANIMATION, 0);
		llSensorRepeat("", "", AGENT, range, PI, 5.0);
	}

	changed(integer change)
	{
		if (change == CHANGED_INVENTORY) {
			appearancenotecard = llGetInventoryName(INVENTORY_NOTECARD, 0);
			animation2play = llGetInventoryName(INVENTORY_ANIMATION, 0);
		}
	}

	sensor(integer num_detected)
	{
		detectedavi = llDetectedKey(0);
		avipos = llDetectedPos(0);
		if (avidetected) {
			// do nothing so theres not a spam of npc's
		}else{
			avidetected = TRUE;
			vector pos = llGetPos() + <0,0,0.5>; // this is so the npc stands on top of the box
			osAgentSaveAppearance(avidetected, appearancenotecard); // this saves your appearance as a xml notecard for the npc to wear.
			npc = osNpcCreate(npcfirstname, npclastname, pos, appearancenotecard);
			llSetObjectDesc((string)npc); // saving the npc's UUID to this prim's description in case of a sim crash/restart

			if (move2avatar && detectedavi != "" && npc != "") {
				osNpcMoveToTarget(npc, avipos, OS_NPC_RUNNING);
				if (animation2play != "") {
					osNpcPlayAnimation(npc, animation2play);
				}
			}else{
				if (animation2play != "") {
					osNpcPlayAnimation(npc, animation2play);
				}
			}
		}
	}

	on_rez(integer start_param)
	{
		llResetScript();
	}

	no_sensor()
	{
		if (avidetected) {
			avidetected = FALSE;
			osNpcStopAnimation(npc, animation2play);
			osNpcRemove(npc);
			npc = "";
			detectedavi = "";
			llSetObjectDesc("");
		}else{
			// do nothing since the npc already derezzed
		}
	}
}
