integer allowall = FALSE; // Setting this to TRUE will allow anyone to open the door. Setting this to FALSE will only let you open the door.
float closetimer = 30.0; // timer for when the door will stay open for before it closes. Setting this to 0.0 will leave the door open untill you touch the door again to close it.

// dont worry about these settings below. The script automaticaly configures these.
key owner;
vector pos;
rotation rot;
integer open;
rotation rot_xyzq;

swing()
{
    rot = llGetRot();
    if (open == FALSE) {
        llSetRot(rot * rot_xyzq);
        open = TRUE;
        llSetTimerEvent(closetimer);
    }else if (open == TRUE) {
        llSetRot(rot / rot_xyzq);
        open = FALSE;
        llSetTimerEvent(0.0);
    }
}

default
{
    on_rez(integer s)
    {
        llResetScript();
    }
    
    state_entry()
    {
        owner = llGetOwner();
        pos = llGetPos();
        rot = llGetRot();
        vector xyz_angles = <0.0,0.0,90.0>; // This is to define a 1 degree change
        vector angles_in_radians = xyz_angles*DEG_TO_RAD; // Change to Radians
        rot_xyzq = llEuler2Rot(angles_in_radians); // Change to a Rotation
        open = FALSE;
    }
    
    touch_end(integer n)
    {
        key toucher = llDetectedKey(0);
        if (allowall == TRUE)
        {
            swing();
        }
        else if (allowall == FALSE && toucher == owner)
        {
            swing();
        }
        else if (allowall == FALSE && toucher != owner)
        {
            llSay(0, "Sorry only " + llKey2Name(owner) + " can open this door.");
        }
    }

    collision_start(integer num_detected)
    {
        key toucher = llDetectedKey(0);
        if (allowall == TRUE)
        {
            swing();
        }
        else if (allowall == FALSE && toucher == owner)
        {
            swing();
        }
        else if (allowall == FALSE && toucher != owner)
        {
            llSay(0, "Sorry only " + llKey2Name(owner) + " can open this door.");
        }
    }

    collision_end(integer num_detected)
    {
        llSleep(5.0);
        swing();
    }
    
    timer()
    {
        if (open == TRUE) {
            swing();
        }
    }
}