integer price = 1; // Set your price here or in the prim's description
vector color = <1.0,1.0,1.0>;
integer Total;
string  OwnerName;
string currency = "Z$";
default
{
    on_rez(integer sparam)
    {
        Total = 0;
        OwnerName = "";
        llResetScript();
    }
    state_entry()
    {
        string checkdesc = llGetObjectDesc();
        if (checkdesc != "") {
            price = (integer)checkdesc;
        }
        integer price2 = price * 10;
        integer price3 = price2 * 10;
        integer price4 = price3 * 10;
        llSetPayPrice(price, [price, price2, price3, price4]);
        OwnerName = llKey2Name(llGetOwner());
        llSetText(OwnerName+"'s Tip Jar.\nAny tips gratefully received!\n"+currency+" 0 Donated so far", color, 1.0);
    }
    money(key id, integer amount)
    {
        Total += amount;
        string str = OwnerName+"'s Tip Jar.\n"+currency+" "+amount+" was last donated by "+llKey2Name(id)+"\n"+currency+" "+Total+" donated so far";
        llSetText(str, color, 1.0);
        llInstantMessage(id,"Thanks for the tip!");
        llInstantMessage(llGetOwner(), llKey2Name(id) + " donated" + currency + (string) amount);
    }
}